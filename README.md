# Gene sequence classification using LSTM

The goal of this project is to generate a model to classify a 60 element DNA sequence into the categories of IE, EI or neither. 


## Overall steps

The project consists of the following steps:

1. Get the data   
2. Prepare the data
3. Load and inspect the data
4. Split into training, validation, and test sets
5. Convert into sequence data required for LSTM
6. Import Keras libraries
7. Study the impact of number of LSTM units
8. Study the impact of dropout
9. Study the impact of recurrent dropout
10. Study the impact of optimization methods
11. Build the final model with the best sets of hyperparameters 
12. Test the performance on the unseen test data

## Get the data

Go to the web site:
https://archive.ics.uci.edu/ml/datasets/Molecular+Biology+(Splice-junction+Gene+Sequences)

Summarizing the description of the data set there:
* This is a data set of DNA sequences that define the boundaries between regions that are spliced out during protein creation (introns) or retained for protein creation (exons).
* There are 3 classes: boundaries between exon and intron sequences (EI), boundaries between intron and exon sequences (IE), or sequences that are neither.
* The dataset has 3190 examples of DNA sequences.
* Each sequence is 60 base-pairs long.

## Detailed Writeup 

Detailed report can be found in [_Gene_sequence_classification_using_LSTM.ipynb_](./Gene_sequence_classification_using_LSTM.ipynb).

## Acknowledgments 

I would like to thank UCI for making the data available for this project. 

